# so1

Trabalho de Sistemas Operacionais I

Hoje é terça. Amanhã é quarta.

## Cabeçalho de Nível 2
Depois de amanhã é Quinta.

### Cabeçalho de nível 3 
Depois de Quinta é sexta.

#### Cabeçalho de Nível 4 
Depois de sexta é sábado.

## Formatação - Itálico e Negrito   
*Johan Liebert*, **O Monstro**.

## Citações
Podemos fazer com um bloco fique destacado utilizando do símbolo de (>)

> Johan Liebert é muito foda.

## Listas
Podemos criar uma listando usando (-) e (.)

## Lista não ordenada
- eae pessoal
- tudo bem
- aqui quem fala
- é o edu

## Lista ordenada
1. Um!
2. Dois!!
3. Três!!!
4. Quatro!!!!

![Johan](Johan.png)